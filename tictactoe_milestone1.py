from random import randint
from random import choice
store=dict()
rnd=1
def display(bd):
    print(bd[0][0]+'|'+bd[0][1]+'|'+bd[0][2])
    print('-|-|-')
    print(bd[1][0]+'|'+bd[1][1]+'|'+bd[1][2])
    print('-|-|-')
    print(bd[2][0]+'|'+bd[2][1]+'|'+bd[2][2])
    print('*'*8)
    print('\n'*3)
    
#ls=[['X','X','O'],['X','O','X'],['O','X','X']]
    
ls=[]
computer_ch=None
human_ch=None
choice=tuple()
available=9
def access():
    """Sets signs"""
    global human_ch
    global computer_ch
    ch=['X','O']
    human_ch=str(input('Do You Want X or O as sign')).upper()
    ch.remove(human_ch)
    computer_ch=ch.pop()
    
    
def comp_play():
    global available
    cont=True
    while cont:
        i=randint(0,2)
        j=randint(0,2)
       
        if ls[i][j]=='X' or ls[i][j]=='O':
            if(available>0 and not who_wins(ls)):
                continue
                
            elif(available>0 and who_wins(ls)):
                print('Computer Won')
                break
            else:
                break
        else:
            if(not who_wins(ls)):
                ls[i][j]=computer_ch
                available=available-1
                cont=False
            
def human_play():
    global available
    cont=True
    while cont:
        i=int(input('Row number'))
        j=int(input('column number'))
        i=i-1
        j=j-1
        if ls[i][j]=='X' or ls[i][j]=='O':
            if(available>0):
                print('Can not change,move already exist')
                continue
            else:
                print('Game over!!!')
                break
        else:
            if(not(who_wins(ls))):
                ls[i][j]=human_ch
                available=available-1
                cont=False
            else:
                who_wins(ls)
                
def who_wins(ls):
    for i in range(3):
        #horizontal
        if(ls[i][0]==ls[i][1]==ls[i][2]==human_ch):
            print('Player has Won')
            return True
        elif(ls[i][0]==ls[i][1]==ls[i][2]==computer_ch):
            print('Computer has Won')
            return True
    for i in range(3):
        #vertical
        if(ls[0][i]==ls[1][i]==ls[2][i]==human_ch):
            print('Player has Won')
            return True
        elif(ls[0][i]==ls[1][i]==ls[2][i]==computer_ch):
            print('Computer has Won')
            return True
    if((ls[0][0]==ls[1][1]==ls[2][2]==human_ch) or (ls[0][2]==ls[1][1]==ls[2][0]==human_ch)):
        print('Player has Won')
        return True
    elif((ls[0][0]==ls[1][1]==ls[2][2]==computer_ch) or (ls[0][2]==ls[1][1]==ls[2][0]==computer_ch)):
        print('Computer has Won')
        return True
    else:
        return False
            
def play_main():
    access()
    global available
    global rnd
    while True:
        if(available>0 and (not who_wins(ls))):
            comp_play()
            print('Computer Play')
            display(ls)
            if(not who_wins(ls)):
                human_play()
                print('Human Play')
            display(ls)
        else:
            print(ls)
            print('All positions are filled')
            store[rnd]=ls
            rnd+=1
            break
def main():
    global ls
    global available
    while True:
        print('Press 1 to Play , Press 2 to Display the board')
        n=int(input())
        if n==1:
            ls=[[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
            play_main()
            available=9
            print('The round',rnd-1,'is complete...')
            print('\n')
        elif n==2:
            rn=int(input('Enter the round number of round'))
            lis=store[rn]
            display(lis)
        
main()
